abstract class Animals {
    boolean eat;
    boolean sleep;

    abstract void animalVoice();

    public void animalSleep(boolean sleep) {
    }

    public void animalEat(boolean eat) {
    }

}
